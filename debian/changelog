libarcus (3.3.0-3) UNRELEASED; urgency=medium

  * Updated standards

 -- Gregor Riepl <onitake@gmail.com>  Tue, 11 Dec 2018 08:09:15 +0100

libarcus (3.3.0-2) unstable; urgency=medium

  [ Gregor Riepl ]
  * Blacklisted incompatible python3-sip version (4.19.11+dfsg-1)
  * Added new symbols that appear with current build dependencies.
    Closes: #914953
  * Depend on python3-dev since we're not building for all Python
    interpreters.
    Closes: #913069

  [ Matthias Klose ]
  * Marked some symbols as optional not seen when building with -O3.
    Closes: #912412

  [ Petter Reinholdtsen ]
  * Corrected incorrectly blacklisting of python3-sip version 4.19.12+dfsg-1.

 -- Gregor Riepl <onitake@gmail.com>  Mon, 10 Dec 2018 11:46:52 +0000

libarcus (3.3.0-1) unstable; urgency=medium

  * New upstream release
  * Migrated to salsa

 -- Gregor Riepl <onitake@gmail.com>  Mon, 18 Jun 2018 19:00:38 +0200

libarcus (3.2.1-1) unstable; urgency=medium

  * New upstream release 3.2.1
  * Marked some more symbols as optional.
    They are not available when compiling with -O3 on ppcle.
    Thanks to Graham Inggs from the Ubuntu project.
  * Set Multi-Arch:same to enable co-installability

 -- Gregor Riepl <onitake@gmail.com>  Fri, 16 Mar 2018 08:52:19 +0100

libarcus (3.1.0-1) unstable; urgency=medium

  * New upstream release 3.1.0

 -- Gregor Riepl <onitake@gmail.com>  Tue, 16 Jan 2018 23:50:49 +0100

libarcus (3.0.3-4) unstable; urgency=medium

  * Added more missing symbols for arch:armel

 -- Gregor Riepl <onitake@gmail.com>  Thu, 30 Nov 2017 08:12:42 +0100

libarcus (3.0.3-3) unstable; urgency=medium

  * Added missing symbols for arch:armel
    Closes: #882495

 -- Gregor Riepl <onitake@gmail.com>  Wed, 29 Nov 2017 18:52:13 +0100

libarcus (3.0.3-2) unstable; urgency=medium

  * Updated symbols file to make the build succeed on other
    architectures than amd64 (Closes: #882495)

 -- Gregor Riepl <onitake@gmail.com>  Fri, 24 Nov 2017 20:19:48 +0100

libarcus (3.0.3-1) unstable; urgency=medium

  [ Gregor Riepl ]
  * New upstream release 3.0.3.
  * Upstream license change to LGPL-3+.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 30 Oct 2017 09:15:52 +0000

libarcus (2.5.0-1) UNRELEASED; urgency=medium

  * Initial Debian release. Closes: #706656

 -- Gregor Riepl <onitake@gmail.com>  Wed, 26 Apr 2017 08:17:37 +0200
